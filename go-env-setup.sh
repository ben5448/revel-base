#!/bin/sh

# Source this file by using the following command: source go-env-setup.sh

OS=`uname`

#check for osx
if [ $OS == "Darwin" ]; then
	BASEDIR=$(cd "$(dirname $BASH_SOURCE)"; pwd)
else
	#check for windows subsystem for linux
	KERNEL=`uname -r`
	if [[ $KERNEL == *"Microsoft"* ]]; then
		BASEDIR=`pwd`
	else
		#default to standard linux
		BASEDIR=`dirname $(readlink -f $BASH_SOURCE)`
	fi

fi

#ensure /go is present
if [[ $BASEDIR == *"/go" ]]; then
	export GOPATH=$BASEDIR
else
	export GOPATH=$BASEDIR/go
fi

export PATH=$PATH:$GOPATH/bin
