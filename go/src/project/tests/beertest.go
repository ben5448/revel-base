package tests

import (
	"github.com/revel/revel/testing"
	// "fmt"
)

type BeerTest struct {
	testing.TestSuite
}

func (t *BeerTest) Before() {
	println("Set up beertest")
}

func (t *BeerTest) TestGetBeerList() {
	t.Get("/beers")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	println(string(t.ResponseBody))
	//fmt.Printf("%#v\n", t.ResponseBody)
}

func (t *BeerTest) TestGetSpecificBeers() {
	t.Get("/beers/1")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	println(string(t.ResponseBody))
	//fmt.Printf("%#v\n", t.ResponseBody)

	t.Get("/beers/2")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	println(string(t.ResponseBody))
	//fmt.Printf("%#v\n", t.ResponseBody)

	t.Get("/beers/3")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
	println(string(t.ResponseBody))
	//fmt.Printf("%#v\n", t.ResponseBody)
}

func (t *BeerTest) After() {
	println("Tear down beertest")
}
