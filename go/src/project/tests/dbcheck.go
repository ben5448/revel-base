package tests

import (
	"encoding/json"
	"bytes"
	"github.com/revel/revel/testing"
)

type DBTest struct {
	testing.TestSuite
}

func (t *DBTest) Before() {
	println("Set up")
}

func (t *DBTest) TestDatabaseVersion() {
	t.Get("/dbcheck")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
}

func (t *DBTest) TestTables() {
	t.Get("/tables")
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
}

func (t *DBTest) TestSchemaTables() {

	type input_constraint struct {
		Schema string
	}
	
	constraints := input_constraint{Schema:"information_schema"}
	jsonBytes, err := json.Marshal(constraints)
	t.Assert(nil == err)
	reader := bytes.NewReader(jsonBytes)
	
	t.Post("/schematables", "application/json", reader)
	
	t.AssertOk()
	t.AssertContentType("application/json; charset=utf-8")
}

func (t *DBTest) After() {
	println("Tear down")
}
