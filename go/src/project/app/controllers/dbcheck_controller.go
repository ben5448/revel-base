package controllers

import(
	"github.com/revel/revel"
	"project/app/database"
)

type DBCheckController struct {
	*revel.Controller
}

func (c DBCheckController) Check() revel.Result {

	type version_info struct {
		Version string
		Error string
	}

	result := version_info{}

	db := &database.Database{Log:c.Log}

	version, err := db.GetVersion()
	if nil != err {
		c.Log.Errorf("Failed to retrieve version. Error %#v\n", err)
		result.Error = err.Error()
	} else {
		result.Version = version
	}

	c.Log.Debugf("Final results %#v\n", result)
	return c.RenderJSON(result)
}

func (c DBCheckController) GetTables() revel.Result {

	type version_info struct {
		Tables []string
		Error string
	}

	result := version_info{}

	db := &database.Database{Log:c.Log}

	tables, err := db.GetTables("")
	if nil != err {
		c.Log.Errorf("Failed to retrieve tables. Error %#v\n", err)
		result.Error = err.Error()
	} else {
		result.Tables = *tables
	}

	c.Log.Debugf("Final results %#v\n", result)
	return c.RenderJSON(result)
}

func (c DBCheckController) GetTablesForSchema() revel.Result {

	type input struct {
		Schema		string
	}

	var constraints input
	c.Params.BindJSON(&constraints)

	type version_info struct {
		Tables []string
		Error string
	}

	result := version_info{}

	db := &database.Database{Log:c.Log}

	tables, err := db.GetTables(constraints.Schema)
	if nil != err {
		c.Log.Errorf("Failed to retrieve tables. Error %#v\n", err)
		result.Error = err.Error()
	} else {
		result.Tables = *tables
	}

	c.Log.Debugf("Final results %#v\n", result)
	return c.RenderJSON(result)
}
