package database

import (
    "github.com/revel/revel"
    "github.com/revel/revel/logger"
    "database/sql"
    "errors"
    "fmt"

    _ "github.com/lib/pq"
)

//method for retrieving the database connection string from app.conf
func ConnectionString() (string, error) {

    //get database host
    host, found := revel.Config.String("database.host")
    if !found {
        return "", errors.New("Database host configuration setting not found")
    }

    //get database port
    port, found := revel.Config.Int("database.port")
    if !found {
        return "", errors.New("Database port configuration setting not found")
    }

    //get database user
    user, found := revel.Config.String("database.user")
    if !found {
        return "", errors.New("Database user configuration setting not found")
    }

    //get database password
    password, found := revel.Config.String("database.password")
    if !found {
        return "", errors.New("Database password configuration setting not found")
    }

    //get database dbname
    dbname, found := revel.Config.String("database.dbname")
    if !found {
        return "", errors.New("Database dbname configuration setting not found")
    }

    return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=require", host, port, user, password, dbname), nil
}

type Database struct {
	Log logger.MultiLogger
}

func (d *Database)Connect() (*sql.DB, error) {

    // Set up the connection string
    psqlInfo, err := ConnectionString()
    if nil != err {
        return nil, err
    }

    // Open database connection
    db, err := sql.Open("postgres", psqlInfo)
    if err != nil {
        d.Log.Infof("Error connecting to database %v", err)
        err = errors.New("Error connecting to database")
    }

    return db, err
}

func (d *Database)GetVersion() (string, error) {
    // Connect to the database
    db, err := d.Connect()
    if nil != db { defer db.Close() }
    if nil != err { return "", err }

    sqlStatement := `SELECT version();`

    var v string
    err = db.QueryRow(sqlStatement).Scan(&v)
    d.Log.Infof("GetVersion with query %v results %v", sqlStatement, err)

    if nil != err {
        d.Log.Errorf("GetVersion with query %v failed with error %v", sqlStatement, err)
        return "", err
    }

    return v, nil
}


func (d *Database)GetTables(schema string) (*[]string, error) {
    // Connect to the database
    db, err := d.Connect()
    if nil != db { defer db.Close() }
    if nil != err { return nil, err }

    var tables []string = make([]string, 0, 100)
    var sqlStatement string
    var rows *sql.Rows

    if len(schema) > 0 {
        sqlStatement = `SELECT table_schema, table_name from INFORMATION_SCHEMA.tables WHERE table_schema = $1 ORDER BY table_schema, table_name;`
        rows, err = db.Query(sqlStatement, schema)
    } else {
        sqlStatement = `SELECT table_schema, table_name from INFORMATION_SCHEMA.tables ORDER BY table_schema, table_name;`
        rows, err = db.Query(sqlStatement)
    }

    d.Log.Infof("GetTables with query %v schema %s results %v", sqlStatement, schema, err)

    if nil != err {
        d.Log.Errorf("GetTables query %v failed with error %v", sqlStatement, err)
        return &tables, err
    }
    defer rows.Close()

    var table_name, schema_name string
    for rows.Next() {
        err = rows.Scan(&schema_name, &table_name)
        if nil != err {
            d.Log.Errorf("GetTables scan from query %v failed with error %v", sqlStatement, err)
            return &tables, err
        }

        tables = append(tables, schema_name + "." + table_name)
    }

    return &tables, nil
}

