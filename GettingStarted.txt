﻿Good starting example: https://nilpath.se/building-a-rest-api-in-golang/
Install go
Install git

Create go-env-setup.sh

#!/bin/sh

# Source this file by using the following command: source go-env-setup.sh

OS=`uname`

#check for osx
if [ $OS == "Darwin" ]; then
	BASEDIR=$(cd "$(dirname $BASH_SOURCE)"; pwd)
else
	#check for windows subsystem for linux
	KERNEL=`uname -r`
	if [[ $KERNEL == *"Microsoft"* ]]; then
		BASEDIR=`pwd`
	else
		#default to standard linux
		BASEDIR=`dirname $(readlink -f $BASH_SOURCE)`
	fi

fi

#ensure /go is present
if [[ $BASEDIR == *"/go" ]]; then
	export GOPATH=$BASEDIR
else
	export GOPATH=$BASEDIR/go
fi


source go-env-setup.sh
go get github.com/revel/revel
go get github.com/revel/cmd/revel

cd go/src
go new projectname

start adding routes, controllers, models and code
